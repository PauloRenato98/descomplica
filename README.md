# descomplica
 Criei uma aplicação bem básica só para conseguirmos acessar um path nela e mostrar algo na tela.

![image](docs/app.PNG)


## Criei três repositórios diferentes para ficar mais organizado, o repo do terraform,helm e aplicação cada um tem sua pipeline.

 ![image](docs/organization.PNG)

## Monitoramento
Acredito que para monitoramento colocaria o datadog para monitor a aplicação como apm, ele pega de ponta a ponta a chamada e é muito bom para fazer troubleshooting. Para monitoramento do cluster colocaria o grafana e tem uma aplicação open-source que gosto bastante, ela envia tudo que acontece no cluster para o slack e de fácil configuração que é o botkube, ele pega todos os eventos e disparam em um canal do slack.

## Adicionais
Tiveram coisas que não consegui fazer em razão do tempo, estou de plantão e fazendo o tcc então tive que fazer uma forma otimizada, tudo que consegui fazer documentei os readme, qualquer dúvida sobre como eu fiz pode entrar em contato comigo também.


